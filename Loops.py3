# if

import socket

i = 11

if i < 10: 
 print("Your IP Address is: ", end="")
 print(socket.gethostbyname(socket.gethostname()))
 print()
elif i == 10:
 print("Nothing happens!")
else:
 print("N/A state")


# while

import cmath
while True:
 print("Enter 'x' for exit.")
 num1 = input("Enter value of a: ")
 num2 = input("Enter value of b: ")
 num3 = input("Enter value of c: ")
 if num1 == 'x':
  break
 else:
  number1 = float(num1)
  number2 = float(num2)
  number3 = float(num3)
  d = (number2 ** 2) - (4 * number1 * number3)
  r1 = (-number2 - cmath.sqrt(d)/(2*number1))
  r2 = (-number2 + cmath.sqrt(d)/(2*number1))
  print("The solutions = {0} and {1}".format(r1, r2))
 
# for

primes = [2, 3, 5, 7, 11, 13, 17]
for prime in primes:
 print(prime)


print("---")
 
for x in range(3, 6):
 print(x)

print("---")

for x in range(3, 8, 1):
 print(x)

print("---")


m1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
m2 = [[10, 11, 12], [13, 14, 15], [16, 17, 18]]
rm = [[0, 0, 0] , [0, 0, 0], [0, 0, 0]]
for i in range(len(m1)):
 for j in range(len(m1[0])):
  rm[i][j] = m1[i][j] + m2[i][j]

for r in rm:
 print(r)