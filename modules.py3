# modules

import os
os.uname()

import platform as p
p.uname()
p.machine()
p.version()
p.platform()
p.system()
p.processor()

print(os.name)

if (os.name == "posix"):
 print(os.system("uname -a"))
else:
 print("unknown OS")

# I/O
# Exceptions

try:
 f = open("testfile", "r")
 f.write("This is my test file for execption handling ...")
except IOError:
 print("Error: can\'t find file or read data")
else:
 print("Written content in the file successfully")
