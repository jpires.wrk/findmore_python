# Variables


# Numbers

x = 10
y = 2
z = 5

res1 = x - y*z
print("RES1=" + str(res1))
res2 = ( x + y ) / z
print("RES2=" + str(res2))
res3 = ( x + y ) // z
print("RES3=" + str(res3))
res4 = ( x + y ) % z
print("RES4=" + str(res4))

#del z
#print(z)

# Strings

text = 'Esta e uma formacao sobre os conceitos basicos de Python'
print(text)
print(text[0])
print(text[11:19])
print(text[7:])
print(text[:10])
print(text * 2)
print(text + " !")

# List <-> array in C -> size and elements can be changed

list1 = [ 1, 2.1, 'jazz']
list2 = [ 0, '']

print("List1:") 
print(list1)
print(list1[1:3])
print(list1[0:])
print("List2:")
print(list2)
#print("List1 x List2:")
#print(list1 * list2)
list2[1] = 1
print("List2:")
print(list2)

# Tuples -> can't be updated -> read-only lists

tuple1 = (1, 2.1, 'jazz')
tuple2 = (0, '')

print("tuple1:")
print(tuple1)
print("tuple2:")
print(tuple2)
print("tuple1 + tuple2:")
print(tuple1 + tuple2)

# Dictionary -> no order!

dict = {}
dict['um'] = "Este e o numero um"
dict[2] = "Este e o numero dois"

print(dict['um'])
print(dict[2])

dict = {'nome': 'Joao', 'idade': 46, 'profissao': 'formador'}
print(dict)
print(dict.keys())
print(dict.values())

# conversion
# look at doc's

# date & time
import time
import calendar

localtime = time.asctime( time.localtime(time.time()))
print("Local current time:", localtime)

cal = calendar.month(2017, 9)
print("Calendar:")
print(cal)
